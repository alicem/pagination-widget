/*
 * Copyright © 2019 Alexander Mikhaylenko <exalm7659@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0+
 */

#include "swipe-tracker.h"

#include <math.h>

#define TOUCHPAD_BASE_DISTANCE 400
#define SCROLL_MULTIPLIER 10
#define MIN_ANIMATION_DURATION 100
#define MAX_ANIMATION_DURATION 400
#define VELOCITY_THRESHOLD 0.4
#define DURATION_MULTIPLIER 3
#define ANIMATION_BASE_VELOCITY 0.002
#define DRAG_THRESHOLD_DISTANCE 5

typedef enum {
  PHOSH_SWIPE_TRACKER_STATE_NONE,
  PHOSH_SWIPE_TRACKER_STATE_PREPARING,
  PHOSH_SWIPE_TRACKER_STATE_PENDING,
  PHOSH_SWIPE_TRACKER_STATE_SCROLLING
} PhoshSwipeTrackerState;

struct _PhoshSwipeTracker
{
  GObject parent_instance;

  GtkWidget *widget;
  gboolean enabled;
  gboolean reversed;
  GtkOrientation orientation;

  guint32 prev_time;
  double velocity;

  double initial_progress;
  double progress;
  gboolean cancelled;
  double cancel_progress;

  double prev_offset;
  double distance;

  double *snap_points;
  int n_snap_points;

  PhoshSwipeTrackerState state;
  GtkGesture *touch_gesture;
};

G_DEFINE_TYPE_WITH_CODE (PhoshSwipeTracker, phosh_swipe_tracker, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_ORIENTABLE, NULL));

enum {
  PROP_0,
  PROP_WIDGET,
  PROP_ENABLED,
  PROP_REVERSED,

  /* GtkOrientable */
  PROP_ORIENTATION,
  N_PROPS = PROP_ORIENTATION
};

static GParamSpec *properties [N_PROPS];

enum {
  SIGNAL_BEGIN,
  SIGNAL_UPDATE,
  SIGNAL_END,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
reset (PhoshSwipeTracker *self)
{
  if (self->snap_points) {
    g_free (self->snap_points);
    self->snap_points = NULL;
  }

  self->state = PHOSH_SWIPE_TRACKER_STATE_NONE;

  self->prev_offset = 0;
  self->distance = 0;

  self->initial_progress = 0;
  self->progress = 0;

  self->prev_time = 0;
  self->velocity = 0;

  self->cancel_progress = 0;

  self->cancelled = FALSE;
}

static void
gesture_prepare (PhoshSwipeTracker *self,
                 double             x,
                 double             y)
{
  if (self->state != PHOSH_SWIPE_TRACKER_STATE_NONE)
    return;

  self->state = PHOSH_SWIPE_TRACKER_STATE_PREPARING;
  g_signal_emit (self, signals[SIGNAL_BEGIN], 0, x, y);
}

static void
gesture_begin (PhoshSwipeTracker *self)
{
  GdkEvent *event;

  if (self->state != PHOSH_SWIPE_TRACKER_STATE_PENDING)
    return;

  event = gtk_get_current_event ();
  self->prev_time = gdk_event_get_time (event);
  self->state = PHOSH_SWIPE_TRACKER_STATE_SCROLLING;
}

static void
gesture_update (PhoshSwipeTracker *self,
                double             delta)
{
  GdkEvent *event;
  guint32 time;
  double progress;
  double first_point, last_point;

  if (self->state != PHOSH_SWIPE_TRACKER_STATE_SCROLLING)
    return;

  event = gtk_get_current_event ();
  time = gdk_event_get_time (event);
  if (time != self->prev_time)
    self->velocity = delta / (time - self->prev_time);

  first_point = self->snap_points[0];
  last_point = self->snap_points[self->n_snap_points - 1];

  progress = self->progress + delta;
  progress = CLAMP (progress, first_point, last_point);

  // FIXME: this is a hack to prevent swiping more than 1 page at once
  progress = CLAMP (progress, self->initial_progress - 1, self->initial_progress + 1);

  self->progress = progress;

  g_signal_emit (self, signals[SIGNAL_UPDATE], 0, progress);

  self->prev_time = time;
}

static void
get_closest_snap_points (PhoshSwipeTracker *self,
                         double            *upper,
                         double            *lower)
{
  int i;

  *upper = 0;
  *lower = 0;

  for (i = 0; i < self->n_snap_points; i++) {
    if (self->snap_points[i] >= self->progress) {
      *upper = self->snap_points[i];
      break;
    }
  }

  for (i = self->n_snap_points - 1; i >= 0; i--) {
    if (self->snap_points[i] <= self->progress) {
      *lower = self->snap_points[i];
      break;
    }
  }
}

static double
get_end_progress (PhoshSwipeTracker *self)
{
  double upper, lower, middle;

  if (self->cancelled)
    return self->cancel_progress;

  get_closest_snap_points (self, &upper, &lower);
  middle = (upper + lower) / 2;

  if (self->progress > middle)
    return (self->velocity * self->distance > -VELOCITY_THRESHOLD ||
            self->initial_progress > upper) ? upper : lower;

  return (self->velocity * self->distance < VELOCITY_THRESHOLD ||
          self->initial_progress < lower) ? lower : upper;
}

static void
gesture_end (PhoshSwipeTracker *self)
{
  double end_progress, velocity;
  int64_t duration;

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_NONE)
    return;

  end_progress = get_end_progress (self);

  velocity = ANIMATION_BASE_VELOCITY;
  if ((end_progress - self->progress) * self->velocity > 0)
    velocity = self->velocity;

  duration = ABS ((self->progress - end_progress) / velocity * DURATION_MULTIPLIER);
  duration = CLAMP (duration, MIN_ANIMATION_DURATION, MAX_ANIMATION_DURATION);

  if (self->cancelled)
    duration = 0;

  g_signal_emit (self, signals[SIGNAL_END], 0, duration, end_progress);
  reset (self);
}

static void
gesture_cancel (PhoshSwipeTracker *self)
{
  if (self->state == PHOSH_SWIPE_TRACKER_STATE_PREPARING) {
    reset (self);
    return;
  }

  if (self->state != PHOSH_SWIPE_TRACKER_STATE_PENDING &&
      self->state != PHOSH_SWIPE_TRACKER_STATE_SCROLLING)
    return;

  self->cancelled = TRUE;
  gesture_end (self);
}

static void
drag_begin_cb (PhoshSwipeTracker *self,
               gdouble            start_x,
               gdouble            start_y,
               GtkGestureDrag    *gesture)
{
  if (self->state != PHOSH_SWIPE_TRACKER_STATE_NONE) {
    gtk_gesture_set_state (self->touch_gesture, GTK_EVENT_SEQUENCE_DENIED);
    return;
  }

  gesture_prepare (self, start_x, start_y);
}

static void
drag_update_cb (PhoshSwipeTracker *self,
                gdouble            offset_x,
                gdouble            offset_y,
                GtkGestureDrag    *gesture)
{
  double offset;
  gboolean is_vertical;

  is_vertical = (self->orientation == GTK_ORIENTATION_VERTICAL);
  if (is_vertical)
    offset = -offset_y / self->distance;
  else
    offset = -offset_x / self->distance;

  if (self->reversed)
    offset = -offset;

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_PENDING) {
    double distance;
    double first_point, last_point;
    gboolean is_offset_vertical, is_overshooting;

    first_point = self->snap_points[0];
    last_point = self->snap_points[self->n_snap_points - 1];

    distance = sqrt (offset_x * offset_x + offset_y * offset_y);
    is_offset_vertical = (ABS (offset_y) > ABS (offset_x));
    is_overshooting = (offset < 0 && self->progress <= first_point) ||
                      (offset > 0 && self->progress >= last_point);

    if (distance >= DRAG_THRESHOLD_DISTANCE) {
      if ((is_vertical == is_offset_vertical) && !is_overshooting) {
        gesture_begin (self);
        gtk_gesture_set_state (self->touch_gesture, GTK_EVENT_SEQUENCE_CLAIMED);
      } else
        gtk_gesture_set_state (self->touch_gesture, GTK_EVENT_SEQUENCE_DENIED);
    }
  }

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_SCROLLING) {
    gesture_update (self, offset - self->prev_offset);
    self->prev_offset = offset;
  }
}

static void
drag_end_cb (PhoshSwipeTracker *self,
             gdouble            offset_x,
             gdouble            offset_y,
             GtkGestureDrag    *gesture)
{
  if (self->state != PHOSH_SWIPE_TRACKER_STATE_SCROLLING) {
    gesture_cancel (self);
    gtk_gesture_set_state (self->touch_gesture, GTK_EVENT_SEQUENCE_DENIED);
    return;
  }

  gesture_end (self);
}

static void
drag_cancel_cb (PhoshSwipeTracker *self,
                GdkEventSequence  *sequence,
                GtkGesture        *gesture)
{
  gesture_cancel (self);
  gtk_gesture_set_state (gesture, GTK_EVENT_SEQUENCE_DENIED);
}

static gboolean
captured_scroll_event (PhoshSwipeTracker *self,
                       GdkEvent          *event)
{
  GdkDevice *source_device;
  GdkInputSource input_source;
  double dx, dy, delta;
  gboolean is_vertical;

  if (gdk_event_get_scroll_direction (event, NULL))
    return GDK_EVENT_PROPAGATE;

  source_device = gdk_event_get_source_device (event);
  input_source = gdk_device_get_source (source_device);
  if (input_source != GDK_SOURCE_TOUCHPAD &&
      input_source != GDK_SOURCE_TRACKPOINT)
    return GDK_EVENT_PROPAGATE;

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_NONE) {
    double root_x, root_y;
    int x, y;
    GtkWidget *toplevel;

    gdk_event_get_root_coords (event, &root_x, &root_y);

    toplevel = gtk_widget_get_toplevel (self->widget);
    gtk_widget_translate_coordinates (toplevel, self->widget, root_x, root_y, &x, &y);

    gesture_prepare (self, x, y);
  }

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_PREPARING) {
    if (gdk_event_is_scroll_stop_event (event))
      gesture_cancel (self);

    return GDK_EVENT_PROPAGATE;
  }

  is_vertical = (self->orientation == GTK_ORIENTATION_VERTICAL);

  gdk_event_get_scroll_deltas (event, &dx, &dy);
  delta = is_vertical ? dy : dx;
  if (self->reversed)
    delta = -delta;

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_PENDING) {
    gboolean is_delta_vertical, is_overshooting;
    double first_point, last_point;

    first_point = self->snap_points[0];
    last_point = self->snap_points[self->n_snap_points - 1];

    is_delta_vertical = (ABS (dy) > ABS (dx));
    is_overshooting = (delta < 0 && self->progress <= first_point) ||
                      (delta > 0 && self->progress >= last_point);

    if ((is_vertical == is_delta_vertical) && !is_overshooting)
      gesture_begin (self);
    else
      gesture_cancel (self);
  }

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_SCROLLING) {
    if (gdk_event_is_scroll_stop_event (event)) {
      gesture_end (self);
    } else {
      self->distance = TOUCHPAD_BASE_DISTANCE;
      gesture_update (self, delta / TOUCHPAD_BASE_DISTANCE * SCROLL_MULTIPLIER);
      return GDK_EVENT_STOP;
    }
  }

  return GDK_EVENT_PROPAGATE;
}

static void
phosh_swipe_tracker_constructed (GObject *object)
{
  PhoshSwipeTracker *self = (PhoshSwipeTracker *)object;

  g_assert (GTK_WIDGET (self->widget));

  gtk_widget_add_events (self->widget,
                         GDK_SMOOTH_SCROLL_MASK |
                         GDK_BUTTON_PRESS_MASK |
                         GDK_BUTTON_RELEASE_MASK |
                         GDK_BUTTON_MOTION_MASK |
                         GDK_TOUCH_MASK |
                         GDK_ALL_EVENTS_MASK);

  self->touch_gesture = g_object_new (GTK_TYPE_GESTURE_DRAG,
                                      "widget", self->widget,
                                      "propagation-phase", GTK_PHASE_NONE,
                                      NULL);

  g_signal_connect_swapped (self->touch_gesture, "drag-begin", G_CALLBACK (drag_begin_cb), self);
  g_signal_connect_swapped (self->touch_gesture, "drag-update", G_CALLBACK (drag_update_cb), self);
  g_signal_connect_swapped (self->touch_gesture, "drag-end", G_CALLBACK (drag_end_cb), self);
  g_signal_connect_swapped (self->touch_gesture, "cancel", G_CALLBACK (drag_cancel_cb), self);

  G_OBJECT_CLASS (phosh_swipe_tracker_parent_class)->constructed (object);
}

static void
phosh_swipe_tracker_finalize (GObject *object)
{
  PhoshSwipeTracker *self = (PhoshSwipeTracker *)object;

  if (self->snap_points)
    g_free (self->snap_points);

  g_signal_handlers_disconnect_by_data (self->touch_gesture, self);
  g_object_unref (self->touch_gesture);
  g_object_unref (self->widget);

  G_OBJECT_CLASS (phosh_swipe_tracker_parent_class)->finalize (object);
}

static void
phosh_swipe_tracker_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  PhoshSwipeTracker *self = PHOSH_SWIPE_TRACKER (object);

  switch (prop_id) {
    case PROP_WIDGET:
      g_value_set_object (value, self->widget);
      break;
    case PROP_ENABLED:
      g_value_set_boolean (value, phosh_swipe_tracker_get_enabled (self));
      break;
    case PROP_REVERSED:
      g_value_set_boolean (value, phosh_swipe_tracker_get_reversed (self));
      break;
    case PROP_ORIENTATION:
      g_value_set_enum (value, self->orientation);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
phosh_swipe_tracker_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  PhoshSwipeTracker *self = PHOSH_SWIPE_TRACKER (object);

  switch (prop_id) {
    case PROP_WIDGET:
      self->widget = GTK_WIDGET (g_object_ref (g_value_get_object (value)));
      break;
    case PROP_ENABLED:
      phosh_swipe_tracker_set_enabled (self, g_value_get_boolean (value));
      break;
    case PROP_REVERSED:
      phosh_swipe_tracker_set_reversed (self, g_value_get_boolean (value));
      break;
    case PROP_ORIENTATION:
      {
        GtkOrientation orientation = g_value_get_enum (value);
        if (orientation != self->orientation) {
          self->orientation = g_value_get_enum (value);
          g_object_notify (G_OBJECT (self), "orientation");
        }
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
phosh_swipe_tracker_class_init (PhoshSwipeTrackerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = phosh_swipe_tracker_constructed;
  object_class->finalize = phosh_swipe_tracker_finalize;
  object_class->get_property = phosh_swipe_tracker_get_property;
  object_class->set_property = phosh_swipe_tracker_set_property;

  properties [PROP_WIDGET] =
    g_param_spec_object ("widget",
                         "Widget",
                         "Widget",
                         GTK_TYPE_WIDGET,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  properties [PROP_ENABLED] =
    g_param_spec_boolean ("enabled",
                          "Enabled",
                          "Enabled",
                          TRUE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  properties [PROP_REVERSED] =
    g_param_spec_boolean ("reversed",
                          "Reversed",
                          "Reversed",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_override_property (object_class,
                                    PROP_ORIENTATION,
                                    "orientation");

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals[SIGNAL_BEGIN] =
    g_signal_new ("begin",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_DOUBLE, G_TYPE_DOUBLE);

  signals[SIGNAL_UPDATE] =
    g_signal_new ("update",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_DOUBLE);

  signals[SIGNAL_END] =
    g_signal_new ("end",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_INT64, G_TYPE_DOUBLE);
}

static void
phosh_swipe_tracker_init (PhoshSwipeTracker *self)
{
  reset (self);
  self->orientation = GTK_ORIENTATION_HORIZONTAL;
  self->enabled = TRUE;
}

PhoshSwipeTracker *
phosh_swipe_tracker_new (GtkWidget *widget)
{
  g_return_val_if_fail (widget != NULL, NULL);

  return g_object_new (PHOSH_TYPE_SWIPE_TRACKER,
                       "widget", widget,
                       NULL);
}

gboolean
phosh_swipe_tracker_get_enabled (PhoshSwipeTracker *self)
{
  g_return_val_if_fail (PHOSH_IS_SWIPE_TRACKER (self), FALSE);

  return self->enabled;
}

void
phosh_swipe_tracker_set_enabled (PhoshSwipeTracker *self,
                                 gboolean           enabled)
{
  g_return_if_fail (PHOSH_IS_SWIPE_TRACKER (self));

  if (self->enabled == enabled)
    return;

  self->enabled = enabled;

  if (!enabled && self->state != PHOSH_SWIPE_TRACKER_STATE_SCROLLING)
    reset (self);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ENABLED]);
}

gboolean
phosh_swipe_tracker_get_reversed (PhoshSwipeTracker *self)
{
  g_return_val_if_fail (PHOSH_IS_SWIPE_TRACKER (self), FALSE);

  return self->reversed;
}

void
phosh_swipe_tracker_set_reversed (PhoshSwipeTracker *self,
                                  gboolean           reversed)
{
  g_return_if_fail (PHOSH_IS_SWIPE_TRACKER (self));

  if (self->reversed == reversed)
    return;

  self->reversed = reversed;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_REVERSED]);
}

gboolean
phosh_swipe_tracker_captured_event (PhoshSwipeTracker *self,
                                    GdkEvent          *event)
{
  GdkEventSequence *sequence;
  gboolean retval;
  GtkEventSequenceState state;

  g_return_val_if_fail (PHOSH_IS_SWIPE_TRACKER (self), GDK_EVENT_PROPAGATE);

  if (!self->enabled && self->state != PHOSH_SWIPE_TRACKER_STATE_SCROLLING)
    return GDK_EVENT_PROPAGATE;

  if (event->type == GDK_SCROLL)
    return captured_scroll_event (self, event);

  if (event->type != GDK_BUTTON_PRESS &&
      event->type != GDK_BUTTON_RELEASE &&
      event->type != GDK_MOTION_NOTIFY &&
      event->type != GDK_TOUCH_BEGIN &&
      event->type != GDK_TOUCH_END &&
      event->type != GDK_TOUCH_UPDATE &&
      event->type != GDK_TOUCH_CANCEL)
    return GDK_EVENT_PROPAGATE;

  sequence = gdk_event_get_event_sequence (event);
  retval = gtk_event_controller_handle_event (GTK_EVENT_CONTROLLER (self->touch_gesture), event);
  state = gtk_gesture_get_sequence_state (self->touch_gesture, sequence);

  if (state == GTK_EVENT_SEQUENCE_DENIED) {
    gtk_event_controller_reset (GTK_EVENT_CONTROLLER (self->touch_gesture));
    return GDK_EVENT_PROPAGATE;
  }

  if (self->state == PHOSH_SWIPE_TRACKER_STATE_SCROLLING)
    return GDK_EVENT_STOP;

  return retval;
}

/**
 * phosh_swipe_tracker_confirm_swipe:
 * @self: a #PhoshSwipeTracker
 * @distance: swipe distance in pixels
 * @snap_points: (array length=n_snap_points) (transfer full): array of snap points
 * @n_snap_points: length of @snap_points
 * @current_progress: initial progress value
 * @cancel_progress:
 */
void
phosh_swipe_tracker_confirm_swipe (PhoshSwipeTracker *self,
                                   double             distance,
                                   double            *snap_points,
                                   int                n_snap_points,
                                   double             current_progress,
                                   double             cancel_progress)
{
  g_return_if_fail (PHOSH_IS_SWIPE_TRACKER (self));

  if (self->state != PHOSH_SWIPE_TRACKER_STATE_PREPARING) {
    gesture_cancel (self);
    return;
  }

  if (self->snap_points)
    g_free (self->snap_points);

  self->distance = distance;
  self->initial_progress = current_progress;
  self->progress = current_progress;
  self->velocity = 0;
  self->snap_points = snap_points;
  self->n_snap_points = n_snap_points;
  self->cancel_progress = cancel_progress;
  self->state = PHOSH_SWIPE_TRACKER_STATE_PENDING;
}

