#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PHOSH_TYPE_DISMISS_BOX (phosh_dismiss_box_get_type())

G_DECLARE_FINAL_TYPE (PhoshDismissBox, phosh_dismiss_box, PHOSH, DISMISS_BOX, GtkEventBox)

PhoshDismissBox *phosh_dismiss_box_new (void);
gboolean         phosh_dismiss_box_get_interactive (PhoshDismissBox *self);
void             phosh_dismiss_box_set_interactive (PhoshDismissBox *self,
                                                    gboolean         interactive);

G_END_DECLS
