private class AppDrawer.WindowRow : Gtk.ListBoxRow {
	public Type window_type { get; construct; }
	public bool dark {get; construct; }

	public WindowRow (string name, Type window_type, bool dark = false) {
		Object (window_type: window_type, dark: dark);

		var label = new Gtk.Label (name);
		label.margin = 12;
		label.xalign = 0;
		add (label);

		show_all ();
	}
}

[GtkTemplate (ui = "/org/gnome/AppDrawer/ui/window.ui")]
public class AppDrawer.Window : Gtk.ApplicationWindow {
	[GtkChild]
	private Gtk.ListBox list;

	construct {
		list.add (new WindowRow ("App Drawer", typeof (AppDrawerWindow), true));
		list.add (new WindowRow ("Sidebar", typeof (SidebarWindow)));
		list.row_activated.connect (row_activated);
	}

	private void row_activated (Gtk.ListBox box, Gtk.ListBoxRow row) {
		var window_row = row as WindowRow;

		var type = window_row.window_type;
		var window = Object.new (type, "application", application, null) as Gtk.ApplicationWindow;
		if (window_row.dark) {
			Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = true;
			window.destroy.connect (() => {
				Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = false;
			});
		}

		window.present ();
	}

	public Window (Gtk.Application app) {
		Object (application: app);
	}
}
