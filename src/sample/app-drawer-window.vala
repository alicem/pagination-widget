[GtkTemplate (ui = "/org/gnome/AppDrawer/ui/app-drawer-window.ui")]
public class AppDrawer.AppDrawerWindow : Gtk.ApplicationWindow {
	[GtkChild]
	private Card card;
	[GtkChild]
	private Gtk.HeaderBar fs_headerbar;
	[GtkChild]
	private Gtk.Button remove_page_btn;
	[GtkChild]
	private Gtk.Button remove_page_btn_fs;
	[GtkChild]
	private Hdy.Drawer drawer;
	[GtkChild]
	private Hdy.Paginator paginator;
	[GtkChild]
	private Gtk.Frame first_page_bin;
	[GtkChild]
	private Gtk.Button panel;
	[GtkChild]
	private Phosh.Arrow arrow;
	[GtkChild]
	private Gtk.Box favorites_box;
	[GtkChild]
	private Gtk.Stack search;
	[GtkChild]
	private Gtk.Button search_button;
	[GtkChild]
	private Gtk.SearchEntry search_entry;
	[GtkChild]
	private Gtk.Box search_view;
	[GtkChild]
	private Hdy.Paginator apps_paginator;
	[GtkChild]
	private Phosh.DismissBox dismiss_box;

	private bool to_search;
	private ulong dismissed_id;

	private void dismissed_cb (Phosh.DismissBox box) {
		apps_paginator.remove (box);
		box.disconnect (dismissed_id);
		dismiss_box = null;
	}

	construct {
		dismissed_id = dismiss_box.dismissed.connect (dismissed_cb);

		paginator.notify["n-pages"].connect (() => {
			remove_page_btn.sensitive = paginator.n_pages > 0;
			remove_page_btn_fs.sensitive = paginator.n_pages > 0;
		});
		paginator.notify["position"].connect (() => {
			drawer.interactive = paginator.position <= 0 && !search_entry.is_focus;
		});
		drawer.notify["progress"].connect (() => {
			var t = (drawer.progress - 1).clamp (0, 1);
			arrow.progress = t;

			if (t == 1) {
				if (to_search) {
					search.visible_child = search_view;
					search_entry.grab_focus ();
					to_search = false;
				}
			} else
				search.visible_child = paginator;
		});

		search_entry.notify["is-focus"].connect (() => {
			drawer.interactive = paginator.position <= 0 && !search_entry.is_focus;
			if (!search_entry.is_focus)
				search.visible_child = paginator;
		});
		search_button.clicked.connect (() => {
			to_search = true;
			drawer.open (search);
		});

		drawer.add_anchor (favorites_box);
		drawer.add_anchor (panel);

		add_page ();
		add_page ();
		add_page ();
	}

	public AppDrawerWindow (Gtk.Application app) {
		Object (application: app);
	}

	[GtkCallback]
	private void add_page () {
		if (first_page_bin.get_child () == null) {
			var page = new SamplePage (0);
			first_page_bin.add (page);
			return;
		}

		var page = new SamplePage ((int) paginator.n_pages);
		page.valign = Gtk.Align.FILL;
		paginator.add (page);
	}

	[GtkCallback]
	private void remove_page () {
		if (paginator.n_pages == 1) {
			first_page_bin.remove (first_page_bin.get_child ());
			return;
		}

		paginator.remove (paginator.get_children ().last ().data);
	}

	[GtkCallback]
	private void on_arrow_clicked () {
		if (drawer.progress >= 2)
			drawer.close ();
		else if (drawer.progress >= 1)
			drawer.open (search);
		else
			drawer.open (favorites_box);
	}

	[GtkCallback]
	private void on_fullscreen () {
		fullscreen ();
		fs_headerbar.show ();
	}

	[GtkCallback]
	private void on_restore () {
		unfullscreen ();
		fs_headerbar.hide ();
	}

	public override void size_allocate (Gtk.Allocation allocation) {
		card.aspect_ratio = (float) allocation.width / allocation.height;
		base.size_allocate (allocation);
	}
}
