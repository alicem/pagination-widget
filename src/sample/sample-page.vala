[GtkTemplate (ui = "/org/gnome/AppDrawer/ui/sample-page.ui")]
public class AppDrawer.SamplePage : Gtk.Bin {
	private static int ROWS = 5;
	private static int COLUMNS = 4;

	[GtkChild]
	private Gtk.FlowBox flowbox;

	public int index {
		construct {
			var first_page = (value == 0);
			var all_apps = (new SampleData ()).all_apps;

			var start_index = first_page ? 0 : COLUMNS * ((ROWS - 1) + ROWS * (value - 1));
			var end_index = start_index + COLUMNS * (first_page ? ROWS - 1 : ROWS);
//			if (end_index > all_apps.length)
//				end_index = all_apps.length;

			for (int i = start_index; i < end_index; i++) {
				var icon = new AppIcon (all_apps[i % all_apps.length]);
				flowbox.add (icon);
			}
		}
	}

	construct {
		flowbox.min_children_per_line = COLUMNS;
		flowbox.max_children_per_line = COLUMNS;
	}

	public SamplePage (int index) {
		Object (index: index);
	}
}
