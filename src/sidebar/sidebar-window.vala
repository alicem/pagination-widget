[GtkTemplate (ui = "/org/gnome/AppDrawer/ui/sidebar-window.ui")]
public class AppDrawer.SidebarWindow : Gtk.ApplicationWindow {
	[GtkChild]
	private Hdy.Drawer drawer;
	[GtkChild]
	private Gtk.ScrolledWindow scrolled_window;
	[GtkChild]
	private Gtk.ListBox list;
	[GtkChild]
	private Gtk.ToggleButton sidebar_button;
	[GtkChild]
	private Gtk.HeaderBar header_bar;
	[GtkChild]
	private Gtk.Label label;

	private bool ignore_toggled;

	private void set_sidebar_button_active (bool active) {
		if (active == sidebar_button.active)
			return;

		ignore_toggled = true;
		sidebar_button.active = active;
	}

	private void update_label () {
		if (get_direction () == Gtk.TextDirection.RTL)
			label.label = "Swipe from the right";
		else
			label.label = "Swipe from the left";
	}

	construct {
		for (int i = 0; i < 20; i++) {
			var label = new Gtk.Label ("Item %d".printf (i));
			label.margin = 12;
			label.xalign = 0;
			label.show ();
			list.add (label);
		}

		update_label ();
		direction_changed.connect (update_label);

		drawer.draw.connect (on_draw);
		drawer.notify["progress"].connect (() => {
			if (drawer.progress <= 0)
				set_sidebar_button_active (false);
			else if (drawer.progress >= 1)
				set_sidebar_button_active (true);
		});

		sidebar_button.toggled.connect (() => {
			if (ignore_toggled) {
				ignore_toggled = false;
				return;
			}

			if (drawer.progress > 0)
				drawer.close ();
			else
				drawer.open (scrolled_window);
		});
		list.row_activated.connect ((list, row) => {
			var label = row.get_children ().first ().data as Gtk.Label;
			header_bar.subtitle = label.label;
			drawer.close ();
		});
	}

	public SidebarWindow (Gtk.Application app) {
		Object (application: app);
	}

	private Gtk.StyleContext create_context (string name, string? klass = null) {
		var path = new Gtk.WidgetPath ();
		var pos = path.append_type (typeof (Gtk.Widget));
		path.iter_set_object_name (pos, name);
		if (klass != null)
			path.iter_add_class (pos, klass);

		var context = new Gtk.StyleContext ();
		context.set_path (path);

		return context;
	}

	private bool on_draw (Gtk.Widget widget, Cairo.Context cr) {
		if (drawer.progress == 0)
			return Gdk.EVENT_PROPAGATE;

		var offset = drawer.get_offset ();
		var width = drawer.get_allocated_width ();
		var height = drawer.get_allocated_height ();
		var is_rtl = (get_direction () == Gtk.TextDirection.RTL );

		var dim_style = create_context ("dimming");
		var shadow_style = create_context ("shadow", is_rtl ? "right" : "left");

		var shadow_width = 0;
		shadow_style.@get (shadow_style.get_state (),
		                   "min-width", out shadow_width,
		                   null);

		cr.save ();

		if (!is_rtl)
			cr.translate (offset, 0);

		cr.push_group ();
		dim_style.render_background (cr, 0, 0, width - offset, height);
		dim_style.render_frame (cr, 0, 0, width - offset, height);
		var group = cr.pop_group ();

		cr.set_source (group);
		cr.paint_with_alpha (drawer.progress);

		if (is_rtl)
			cr.translate (width - offset - shadow_width, 0);

		cr.push_group ();
		shadow_style.render_background (cr, 0, 0, shadow_width, height);
		shadow_style.render_frame (cr, 0, 0, shadow_width, height);
		group = cr.pop_group ();

		cr.set_source (group);
		cr.paint_with_alpha (double.min ((double) offset / shadow_width, 1));

		cr.restore ();

		return Gdk.EVENT_PROPAGATE;
	}
}
